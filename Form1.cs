﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;


namespace Diziler_KayitEkleme_Listeleme_Arama
{
    public partial class Form1 : Form
    {
        int sayac=0;
        string[] tc= new string [100];
        string[] ad = new string[100];
        string[] soyad = new string[100];
        string[] bolum = new string[100];

        string[] sinif = new string[100];

        string[] tel = new string[100];

        string[] cinsiyet = new string[100];


        



        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {



            if (txttcno.Text.Trim() != "" && txtad.Text.Trim() != string.Empty && txtsoyad.Text.Trim() != string.Empty && cmbbolum.SelectedItem.ToString() != string.Empty && cmbsinif.SelectedItem.ToString() != string.Empty && mtxttel.Text.Trim() != string.Empty && (rbkadin.Checked == true || rberkek.Checked == true))
            {
                tc[sayac] = txttcno.Text;
                ad[sayac] = txtad.Text;
                soyad[sayac] = txtsoyad.Text;
                bolum[sayac] = cmbbolum.SelectedItem.ToString();
                sinif[sayac] = cmbsinif.Text;
                tel[sayac] = mtxttel.Text;

                if (rbkadin.Checked == true)
                {
                    cinsiyet[sayac] = "Kadin";
                }
                else
                {
                    cinsiyet[sayac] = "Erkek";
                }
            }
            else
            {
                MessageBox.Show("bos alan bırakmayınız");
            }
            lbltamam.Text = "Kayıt Tamamlandı";
            sayac++;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void btntemizle_Click(object sender, EventArgs e)
        {
            txtad.Clear();
            txttcno.Clear();
            txtsoyad.Clear();
            cmbbolum.ResetText();
            cmbsinif.ResetText();
            mtxttel.Clear();
            rbkadin.ResetText();
            rberkek.ResetText();
            lbltamam.Text = "";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            for (int i = 0; i < sayac; i++)
            {
                if (tc[i]!=string.Empty)
                {
                    listBox1.Items.Add(tc[i] + "\t" + ad[i] + "\t" + soyad[i] + "\t" + bolum[i] + "\t" + sinif[i] + "\t" + tel[i] + "\t" + cinsiyet[i] + "\t");
                }
                

            }
        }

        private void btnara_Click(object sender, EventArgs e)
        {
            string aranan = Interaction.InputBox("Aranan kişinin tcsini giriniz:", "kayıt arama");

            for (int i = 0; i < sayac; i++)
            {
                if (aranan.Trim()==tc[i])
                {
                    lblara_tc.Text = tc[i];
                    lblara_ad.Text = ad[i];
                    lblara_soyad.Text = soyad[i];
                    lblara_bolum.Text = bolum[i];
                    lblara_sinif.Text = sinif[i];
                    lblara_tel.Text = tel[i];
                    lblara_cinsiyet.Text = cinsiyet[i];



                }
                else
                {
                    MessageBox.Show("Kayıt Bulunamadı");
                }


                groupBox4.Visible = true;
            }


           

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            groupBox4.Visible = false;
        }
    }
}
