﻿namespace Diziler_KayitEkleme_Listeleme_Arama
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbltamam = new System.Windows.Forms.Label();
            this.btntemizle = new System.Windows.Forms.Button();
            this.btnkaydet = new System.Windows.Forms.Button();
            this.rberkek = new System.Windows.Forms.RadioButton();
            this.rbkadin = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.mtxttel = new System.Windows.Forms.MaskedTextBox();
            this.txtsoyad = new System.Windows.Forms.TextBox();
            this.txtad = new System.Windows.Forms.TextBox();
            this.txttcno = new System.Windows.Forms.TextBox();
            this.cmbbolum = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbsinif = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblara_cinsiyet = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblara_tel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblara_sinif = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblara_bolum = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblara_soyad = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblara_ad = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblara_tc = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnara = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbltamam);
            this.groupBox1.Controls.Add(this.btntemizle);
            this.groupBox1.Controls.Add(this.btnkaydet);
            this.groupBox1.Controls.Add(this.rberkek);
            this.groupBox1.Controls.Add(this.rbkadin);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.mtxttel);
            this.groupBox1.Controls.Add(this.txtsoyad);
            this.groupBox1.Controls.Add(this.txtad);
            this.groupBox1.Controls.Add(this.txttcno);
            this.groupBox1.Controls.Add(this.cmbbolum);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbsinif);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(360, 298);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kayıt Ekle";
            // 
            // lbltamam
            // 
            this.lbltamam.AutoSize = true;
            this.lbltamam.Location = new System.Drawing.Point(68, 273);
            this.lbltamam.Name = "lbltamam";
            this.lbltamam.Size = new System.Drawing.Size(103, 13);
            this.lbltamam.TabIndex = 17;
            this.lbltamam.Text = "................................";
            // 
            // btntemizle
            // 
            this.btntemizle.Location = new System.Drawing.Point(128, 235);
            this.btntemizle.Name = "btntemizle";
            this.btntemizle.Size = new System.Drawing.Size(75, 23);
            this.btntemizle.TabIndex = 16;
            this.btntemizle.Text = "Temizle";
            this.btntemizle.UseVisualStyleBackColor = true;
            this.btntemizle.Click += new System.EventHandler(this.btntemizle_Click);
            // 
            // btnkaydet
            // 
            this.btnkaydet.Location = new System.Drawing.Point(38, 235);
            this.btnkaydet.Name = "btnkaydet";
            this.btnkaydet.Size = new System.Drawing.Size(75, 23);
            this.btnkaydet.TabIndex = 15;
            this.btnkaydet.Text = "Kaydet";
            this.btnkaydet.UseVisualStyleBackColor = true;
            this.btnkaydet.Click += new System.EventHandler(this.button1_Click);
            // 
            // rberkek
            // 
            this.rberkek.AutoSize = true;
            this.rberkek.Location = new System.Drawing.Point(177, 212);
            this.rberkek.Name = "rberkek";
            this.rberkek.Size = new System.Drawing.Size(53, 17);
            this.rberkek.TabIndex = 14;
            this.rberkek.TabStop = true;
            this.rberkek.Text = "Erkek";
            this.rberkek.UseVisualStyleBackColor = true;
            // 
            // rbkadin
            // 
            this.rbkadin.AutoSize = true;
            this.rbkadin.Location = new System.Drawing.Point(81, 212);
            this.rbkadin.Name = "rbkadin";
            this.rbkadin.Size = new System.Drawing.Size(52, 17);
            this.rbkadin.TabIndex = 13;
            this.rbkadin.TabStop = true;
            this.rbkadin.Text = "Kadın";
            this.rbkadin.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Cinsiyet:";
            // 
            // mtxttel
            // 
            this.mtxttel.Location = new System.Drawing.Point(81, 173);
            this.mtxttel.Mask = "(999) 000-0000";
            this.mtxttel.Name = "mtxttel";
            this.mtxttel.Size = new System.Drawing.Size(100, 20);
            this.mtxttel.TabIndex = 11;
            // 
            // txtsoyad
            // 
            this.txtsoyad.Location = new System.Drawing.Point(81, 66);
            this.txtsoyad.Name = "txtsoyad";
            this.txtsoyad.Size = new System.Drawing.Size(100, 20);
            this.txtsoyad.TabIndex = 10;
            this.txtsoyad.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // txtad
            // 
            this.txtad.Location = new System.Drawing.Point(81, 43);
            this.txtad.Name = "txtad";
            this.txtad.Size = new System.Drawing.Size(100, 20);
            this.txtad.TabIndex = 9;
            // 
            // txttcno
            // 
            this.txttcno.Location = new System.Drawing.Point(81, 17);
            this.txttcno.Name = "txttcno";
            this.txttcno.Size = new System.Drawing.Size(100, 20);
            this.txttcno.TabIndex = 8;
            // 
            // cmbbolum
            // 
            this.cmbbolum.FormattingEnabled = true;
            this.cmbbolum.Items.AddRange(new object[] {
            "BilişimTeknolojileri",
            "Makine Teknolojisi",
            "Kimya Teknolojisi",
            "Elektrik Elektronik Teknolojisi",
            "Yenilenebilir Enerji Teknolojileri"});
            this.cmbbolum.Location = new System.Drawing.Point(81, 102);
            this.cmbbolum.Name = "cmbbolum";
            this.cmbbolum.Size = new System.Drawing.Size(121, 21);
            this.cmbbolum.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Tel:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Sınıf:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Bölüm:";
            // 
            // cmbsinif
            // 
            this.cmbsinif.FormattingEnabled = true;
            this.cmbsinif.Items.AddRange(new object[] {
            "9",
            "10",
            "11",
            "12"});
            this.cmbsinif.Location = new System.Drawing.Point(81, 141);
            this.cmbsinif.Name = "cmbsinif";
            this.cmbsinif.Size = new System.Drawing.Size(121, 21);
            this.cmbsinif.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Soyad:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ad:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tc Kimlik No:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.btnara);
            this.groupBox2.Location = new System.Drawing.Point(394, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(336, 298);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kayıt Ara";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblara_cinsiyet);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.lblara_tel);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.lblara_sinif);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.lblara_bolum);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.lblara_soyad);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.lblara_ad);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.lblara_tc);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Location = new System.Drawing.Point(18, 49);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(302, 243);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            // 
            // lblara_cinsiyet
            // 
            this.lblara_cinsiyet.AutoSize = true;
            this.lblara_cinsiyet.Location = new System.Drawing.Point(125, 193);
            this.lblara_cinsiyet.Name = "lblara_cinsiyet";
            this.lblara_cinsiyet.Size = new System.Drawing.Size(22, 13);
            this.lblara_cinsiyet.TabIndex = 14;
            this.lblara_cinsiyet.Text = ".....";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Tc Kimil No:";
            // 
            // lblara_tel
            // 
            this.lblara_tel.AutoSize = true;
            this.lblara_tel.Location = new System.Drawing.Point(125, 166);
            this.lblara_tel.Name = "lblara_tel";
            this.lblara_tel.Size = new System.Drawing.Size(22, 13);
            this.lblara_tel.TabIndex = 13;
            this.lblara_tel.Text = ".....";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 41);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Ad:";
            // 
            // lblara_sinif
            // 
            this.lblara_sinif.AutoSize = true;
            this.lblara_sinif.Location = new System.Drawing.Point(125, 134);
            this.lblara_sinif.Name = "lblara_sinif";
            this.lblara_sinif.Size = new System.Drawing.Size(22, 13);
            this.lblara_sinif.TabIndex = 12;
            this.lblara_sinif.Text = ".....";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Soyad:";
            // 
            // lblara_bolum
            // 
            this.lblara_bolum.AutoSize = true;
            this.lblara_bolum.Location = new System.Drawing.Point(125, 104);
            this.lblara_bolum.Name = "lblara_bolum";
            this.lblara_bolum.Size = new System.Drawing.Size(22, 13);
            this.lblara_bolum.TabIndex = 11;
            this.lblara_bolum.Text = ".....";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 104);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Bölüm:";
            // 
            // lblara_soyad
            // 
            this.lblara_soyad.AutoSize = true;
            this.lblara_soyad.Location = new System.Drawing.Point(125, 75);
            this.lblara_soyad.Name = "lblara_soyad";
            this.lblara_soyad.Size = new System.Drawing.Size(22, 13);
            this.lblara_soyad.TabIndex = 10;
            this.lblara_soyad.Text = ".....";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 134);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Sınıf:";
            // 
            // lblara_ad
            // 
            this.lblara_ad.AutoSize = true;
            this.lblara_ad.Location = new System.Drawing.Point(125, 36);
            this.lblara_ad.Name = "lblara_ad";
            this.lblara_ad.Size = new System.Drawing.Size(22, 13);
            this.lblara_ad.TabIndex = 9;
            this.lblara_ad.Text = ".....";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 166);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Tel:";
            // 
            // lblara_tc
            // 
            this.lblara_tc.AutoSize = true;
            this.lblara_tc.Location = new System.Drawing.Point(125, 13);
            this.lblara_tc.Name = "lblara_tc";
            this.lblara_tc.Size = new System.Drawing.Size(22, 13);
            this.lblara_tc.TabIndex = 8;
            this.lblara_tc.Text = ".....";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 193);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "Cinsiyet:";
            // 
            // btnara
            // 
            this.btnara.Location = new System.Drawing.Point(18, 20);
            this.btnara.Name = "btnara";
            this.btnara.Size = new System.Drawing.Size(302, 23);
            this.btnara.TabIndex = 0;
            this.btnara.Text = "Kayıt Ara";
            this.btnara.UseVisualStyleBackColor = true;
            this.btnara.Click += new System.EventHandler(this.btnara_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.listBox1);
            this.groupBox3.Location = new System.Drawing.Point(13, 317);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(717, 214);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kayıtları Listele";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(688, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Kayıtları Listele";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(13, 54);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(688, 147);
            this.listBox1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 543);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbltamam;
        private System.Windows.Forms.Button btntemizle;
        private System.Windows.Forms.Button btnkaydet;
        private System.Windows.Forms.RadioButton rberkek;
        private System.Windows.Forms.RadioButton rbkadin;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox mtxttel;
        private System.Windows.Forms.TextBox txtsoyad;
        private System.Windows.Forms.TextBox txtad;
        private System.Windows.Forms.TextBox txttcno;
        private System.Windows.Forms.ComboBox cmbbolum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbsinif;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnara;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label lblara_cinsiyet;
        private System.Windows.Forms.Label lblara_tel;
        private System.Windows.Forms.Label lblara_sinif;
        private System.Windows.Forms.Label lblara_bolum;
        private System.Windows.Forms.Label lblara_soyad;
        private System.Windows.Forms.Label lblara_ad;
        private System.Windows.Forms.Label lblara_tc;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}

